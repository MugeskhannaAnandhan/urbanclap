import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Header from './components/layouts/header';
import Sidebar from './components/layouts/sidebar';
import Footer from './components/layouts/footer';
import Dashboard from './components/Dashboard/dashboard';

import addFaq from './components/Faqs/addFaq';
import listFaqs from './components/Faqs/listFaqs';
import editFaq from './components/Faqs/editFaq';
import viewFaq from './components/Faqs/viewFaq';

import addPage from './components/Pages/addPage';
import listPages from './components/Pages/listPages';
import editPage from './components/Pages/editPage';
import viewPage from './components/Pages/viewPage';

import addCountry from './components/Country/addCountry';
import listCountries from './components/Country/listCountries';
import editCountry from './components/Country/editCountry';
import viewCountry from './components/Country/viewCountry';

import addImage from './components/Category/addImage';
import listImages from './components/Category/listImages';
import viewImage from './components/Category/viewImage';
import editImage from './components/Category/editImage';

import addCurrency from './components/Currency/addCurrency';
import listCurrencies from './components/Currency/listCurrencies';
import viewCurrency from './components/Currency/viewCurrency';
import editCurrency from './components/Currency/editCurrency';

import addLanguage from './components/Language/addLanguage';
import listLanguages from './components/Language/listLanguages';
import editLanguage from './components/Language/editLanguage';
import viewLanguage from './components/Language/viewLanguage';

function App() {
  return (
    <>
      <Router>
        <div class="container-scroller ">
          <Header />
          <div class="container-fluid page-body-wrapper">
            <Sidebar />
            <div class="main-panel">
              <Switch>

                <Route path="/dashboard" component={Dashboard} />

                <Route path="/addFaq" component={addFaq} />
                <Route path="/listFaqs" component={listFaqs} />
                <Route path="/editFaq/:id" component={editFaq} />
                <Route path="/viewFaq/:id" component={viewFaq} />

                <Route path="/addPage" component={addPage} />
                <Route path="/listPages" component={listPages} />
                <Route path="/editPage/:id" component={editPage} />
                <Route path="/viewPage/:id" component={viewPage} />

                <Route path="/addCountry" component={addCountry} />
                <Route path="/listCountries" component={listCountries} />
                <Route path="/editCountry/:id" component={editCountry} />
                <Route path="/viewCountry/:id" component={viewCountry} />

                <Route path="/addImage" component={addImage} />
                <Route path="/listImages" component={listImages} />
                <Route path="/viewImage/:id" component={viewImage} />
                <Route path="/editImage/:id" component={editImage} />

                <Route path="/addCurrency" component={addCurrency} />
                <Route path="/listCurrencies" component={listCurrencies} />
                <Route path="/viewCurrency/:id" component={viewCurrency} />
                <Route path="/editCurrency/:id" component={editCurrency} />

                <Route path="/addLanguage" component={addLanguage} />
                <Route path="/listLanguages" component={listLanguages} />
                <Route path="/editLanguage/:id" component={editLanguage} />
                <Route path="/viewLanguage/:id" component={viewLanguage} />



              </Switch>
            </div>
          </div>
          <Footer />
        </div>
      </Router>
    </>
  );
}

export default App;
