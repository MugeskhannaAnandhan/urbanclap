const ADMINURL = "http://localhost:8000/";
const ROOTURL = ADMINURL + "api/v1";

const API = {
    "addFaq": ROOTURL + "/faqs/addUpdateFaq",
    "viewFaq": ROOTURL + "/faqs/viewFaq",
    "listFaqs": ROOTURL + "/faqs/listFaqs",
    "deleteFaq": ROOTURL + "/faqs/deleteFaq",
    "addPage": ROOTURL + "/pages/addUpdatePage",
    "viewPage": ROOTURL + "/pages/viewPage",
    "listPages": ROOTURL + "/pages/listPages",
    "deletePage": ROOTURL + "/pages/deletePage",
    "addCountry": ROOTURL + "/countries/addUpdateCountry",
    "viewCountry": ROOTURL + "/countries/viewCountry",
    "listCountries": ROOTURL + "/countries/listCountries",
    "deleteCountry": ROOTURL + "/countries/deleteCountry",
    "addUploadImage": ROOTURL + "/categories/addUploadImage",
    "listImages": ROOTURL + "/categories/listImages",
    "viewImage": ROOTURL + "/categories/viewImage",
    "deleteImage": ROOTURL + "/categories/deleteImage",
    "addCurrency": ROOTURL + "/currencies/addUpdateCurrency",
    "listCurrencies": ROOTURL + "/currencies/listCurrencies",
    "deleteCurrency": ROOTURL + "/currencies/deleteCurrency",
    "viewCurrency": ROOTURL + "/currencies/viewCurrency",
    "addLanguage": ROOTURL + "/languages/addUpdateLanguage",
    "listLanguages": ROOTURL + "/languages/listLanguages",
    "deleteLanguage": ROOTURL + "/languages/deleteLanguage",
    "viewLanguage": ROOTURL + "/languages/viewLanguage",

}

export default URL = {
    API: API,
    ADMINURL: ADMINURL
}
