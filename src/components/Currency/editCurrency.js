import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_CURRENCY, AC_LIST_CURRENCIES, AC_VIEW_CURRENCY,AC_HANDLE_INPUT_CHANGE } from '../actions/currency';
import swal from 'sweetalert';
import { Redirect } from 'react-router-dom';

class editCurrency extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            codeError: false,
            name: '',
            nameError: false,
            status: '',
            statusError: false,
            editStatus: false

        }
        this.validation = this.validation.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.back = this.back.bind(this);

    }
    back(){
        this.setState({editStatus:true});
    }

    validation() {
        const name = this.props.currencyReducer.currencyInfo.name;
        const code = this.props.currencyReducer.currencyInfo.code;
        const status = this.props.currencyReducer.currencyInfo.status;
        const id = this.props.currencyReducer.currencyInfo.id;
        let formData = {
            name : name,
            code : code,
            status : status,
            id : id
        }
        console.log("-=-formData=-=-",formData);
        this.props.AC_ADD_CURRENCY(formData);

    }
    handleInputChange(event) {
        let name = event.target.id;
        let value = event.target.value;
        this.props.AC_HANDLE_INPUT_CHANGE(name,value);
    }

    componentWillMount() {
        let currencyId = this.props.match.params.id;
        let formData = {id: currencyId}
        this.props.AC_VIEW_CURRENCY(formData);
    }

    render() {
        const name = this.props.currencyReducer.currencyInfo.name;
        const code = this.props.currencyReducer.currencyInfo.code;
        const status = this.props.currencyReducer.currencyInfo.status;
        if(this.state.editStatus){
            return <Redirect to ='/listCurrencies'/>
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Edit Currency</h4>
                                <form className="forms-sample" id="editCurrency">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">NAME</label>
                                        <input type="text" autoComplete='off' placeholder="Name" id="name" value={name} onChange={this.handleInputChange} style={{ borderColor: this.state.color0 }} className="form-control" />
                                        {this.state.nameError ? <label className="mt-2" style={{ color: 'red' }}>Name is required</label> : ""}
                                        {this.state.nameCountError ? <label className="mt-2" style={{ color: 'red' }}>Name should be atleast 3 characters</label> : ""}

                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">CODE</label>
                                        <input type="text" autoComplete='off' placeholder="Code" id="code" value={code} onChange={this.handleInputChange} style={{ borderColor: this.state.color1 }} className="form-control" />
                                        {this.state.codeError ? <label className="mt-2" style={{ color: 'red' }}>Code is required</label> : ""}
                                        {this.state.codeCountError ? <label className="mt-2" style={{ color: 'red' }}>Code should be atleast 2 characters</label> : ""}

                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">STATUS</label>
                                        <select className="form-control" id="status"value={status} style={{ backgroundColor: 'white' }} onChange={this.handleInputChange} >
                                            <option value="Status">Select Status</option>
                                            <option value="active" selected ={status ==true} >Active</option>
                                            <option value="inactive" selected={status == false}>Inactive</option>
                                        </select>
                                        {this.state.statusError ? <label className="mt-2" style={{ color: 'red' }}>Status is required</label> : ""}
                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{ backgroundColor: 'blue',color: 'white'}} onClick={this.validation}>Submit</button>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{ backgroundColor: 'blue',color: 'white', width:"100px"}} onClick={this.back}>Back</button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log('map state', state);
    return {
        currencyReducer: state.currencyReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_CURRENCY, AC_LIST_CURRENCIES, AC_VIEW_CURRENCY, AC_HANDLE_INPUT_CHANGE }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(editCurrency);
