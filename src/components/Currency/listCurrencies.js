import React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_CURRENCY, AC_LIST_CURRENCIES,AC_DELETE_CURRENCY } from '../actions/currency';
import swal from 'sweetalert';
import { Redirect} from "react-router-dom";

class listCurrencies extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editStatus: false,
            viewStatus: false
        }
        this.deleteCurrency = this.deleteCurrency.bind(this);
        this.editCurrency = this.editCurrency.bind(this);
        this.viewCurrency = this.viewCurrency.bind(this);
    }
    componentDidMount() {
        this.props.AC_LIST_CURRENCIES();
        this.props.AC_ADD_CURRENCY();
        this.props.AC_DELETE_CURRENCY();

    }

    editCurrency(event){
        let currencyId = event.target.id;
        this.setState({ editStatus: true, editId: currencyId })
    }
    viewCurrency(event){
        let currencyId = event.target.id;
        this.setState({ viewStatus: true, viewId: currencyId })
    }
    deleteCurrency(event) {
        let currencyId = event.target.id;
        var formData = {
            id: currencyId,
        }
        swal({
            title: "Are you sure?",
            text: "Do you want to Delete the Currency!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.AC_LIST_CURRENCIES();
                    this.props.AC_DELETE_CURRENCY(formData);
                    this.props.AC_LIST_CURRENCIES();
                    swal("Currency Deleted Successfully!", {
                        buttons: false,
                        timer: 2000,
                      });
                } 
            });
    }

    render() {
        if (this.state.editStatus) {
            return <Redirect to={'/editCurrency'+'/'+this.state.editId} />
        }
        if (this.state.viewStatus) {
            return <Redirect to={'/viewCurrency'+'/'+this.state.viewId} />
        }

        var listCurrency = this.props.currencyReducer.listCurrencies;
        var activeCount = 0;
        var inactiveCount = 0;
        var resultArray = [];
        if (listCurrency !== undefined) {
            if (listCurrency.length > 0) {
                for (var i = 0; i < listCurrency.length; i++) {
                    if (listCurrency[i].status == "active") {
                        activeCount++;
                    }
                    else {
                        inactiveCount++;
                    }
                    resultArray.push(
                        <tr key={listCurrency[i]._id}>
                            <th scope="row">{i + 1}</th>
                            <td>{listCurrency[i].name}</td>
                            <td>{listCurrency[i].code}</td>
                            <td>{listCurrency[i].status}</td>
                            <td>
                                <i class="menu-icon mdi mdi-tooltip-edit text-primary me-2" id={listCurrency[i]._id} onClick={this.editCurrency} style={{fontSize: "22px"}}></i>
                                <i class="menu-icon mdi mdi mdi-view-list text-primary me-2" id={listCurrency[i]._id} onClick={this.viewCurrency} style={{fontSize: "25px" }}></i>
                                <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={listCurrency[i]._id} onClick={this.deleteCurrency} style={{fontSize: "25px" }}></i>
                            </td>
                        </tr>
                    )
                }
            }
        }

        

        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total Currency</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{listCurrency.length}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage">{activeCount}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage">{inactiveCount}</h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{marginBottom: "275px"}}>
                    <thead>
                    <h4 className="card-title"><b>List Currencies</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Code</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>
            </>

        );
    }
}



function mapStateToProps(state) {
    console.log('map state', state);
    return {
        currencyReducer: state.currencyReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_CURRENCY, AC_LIST_CURRENCIES,AC_DELETE_CURRENCY }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(listCurrencies);