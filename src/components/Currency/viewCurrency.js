import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_CURRENCY, AC_LIST_CURRENCIES, AC_VIEW_CURRENCY,AC_HANDLE_INPUT_CHANGE } from '../actions/currency';
import { Redirect } from 'react-router-dom';


class viewCurrency extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viewStatus:false,
        }
        this.back = this.back.bind(this);
    }

    back(){
        this.setState({viewStatus:true});
    }
    componentWillMount() {
        let currencyId = this.props.match.params.id;
        let formData = {id: currencyId}
        this.props.AC_VIEW_CURRENCY(formData);
    }

    render() {
        const name = this.props.currencyReducer.currencyInfo.name;
        const code = this.props.currencyReducer.currencyInfo.code;
        const status = this.props.currencyReducer.currencyInfo.status;
        if(this.state.viewStatus){
            return <Redirect to ='/listCurrencies'/>
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">View Currency</h4>
                                <form className="forms-sample" id="viewCurrency">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">NAME</label>
                                        <input type="text" autoComplete='off' placeholder="Name" id="name" value={name} className="form-control" disabled/>
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">CODE</label>
                                        <input type="text" autoComplete='off' placeholder="Code" id="code" value={code} className="form-control"disabled />
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">STATUS</label>
                                        <select className="form-control" id="status" value={status} style={{ backgroundColor: 'white' }} disabled>
                                            <option value="Status">Select Status</option>
                                            <option value="active" selected ={status ==true} >Active</option>
                                            <option value="inactive" selected={status == false}>Inactive</option>
                                        </select>
                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{
                                        backgroundColor: 'blue',
                                        color: 'white'
                                    }} onClick={this.back}>Back</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log('map state', state);
    return {
        currencyReducer: state.currencyReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_CURRENCY, AC_LIST_CURRENCIES, AC_VIEW_CURRENCY, AC_HANDLE_INPUT_CHANGE }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(viewCurrency);
