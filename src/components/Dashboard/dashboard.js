import React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_LIST_FAQS } from '../actions/faqs';
import { AC_LIST_COUNTRIES } from '../actions/country';
import { AC_LIST_IMAGES } from '../actions/category';
import { AC_LIST_PAGES } from '../actions/pages';
import { AC_LIST_CURRENCIES } from '../actions/currency';
import { AC_LIST_LANGUAGES } from "../actions/language";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        this.props.AC_LIST_COUNTRIES();
        this.props.AC_LIST_FAQS();
        this.props.AC_LIST_IMAGES();
        this.props.AC_LIST_PAGES();
        this.props.AC_LIST_CURRENCIES();
        this.props.AC_LIST_LANGUAGES();


    }
    render() {
        var listFaq = this.props.faqsReducer.listFaqs;
        var listCountry = this.props.countryReducer.listCountries;
        var listImage = this.props.imagesReducer.listImages;
        var listPage = this.props.pagesReducer.listPages;
        var listCurrency = this.props.currencyReducer.listCurrencies;
        var listLanguage = this.props.languagesReducer.listLanguages;
        return (
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="home-tab">
                            <div class="tab-content tab-content-basic">
                                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                                <div>
                                                    <p class="statistics-title"><b>Total FAQS</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listFaq.length}</h3>
                                                </div>
                                                <div>
                                                    <p class="statistics-title"><b>Total Pages</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listPage.length}</h3>
                                                </div>
                                                <div>
                                                    <p class="statistics-title"><b>Total Countries</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listCountry.length}</h3>
                                                </div>
                                                <div>
                                                    <p class="statistics-title"><b>Total Categories</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listImage.length}</h3>
                                                </div>
                                                <div>
                                                    <p class="statistics-title"><b>Total Currencies</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listCurrency.length}</h3>
                                                </div>
                                                <div>
                                                    <p class="statistics-title"><b>Total Languages</b></p>
                                                    <h3 class="rate-percentage" style={{ textAlign: "center" }}>{listLanguage.length}</h3>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log('map state', state);
    return {
        faqsReducer: state.faqsReducer,
        countryReducer: state.countryReducer,
        imagesReducer: state.imagesReducer,
        pagesReducer: state.pagesReducer,
        currencyReducer: state.currencyReducer,
        languagesReducer: state.languagesReducer,
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_LIST_COUNTRIES, AC_LIST_FAQS, AC_LIST_IMAGES, AC_LIST_PAGES, AC_LIST_CURRENCIES, AC_LIST_LANGUAGES }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);