import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_LIST_FAQS } from '../actions/faqs';
import { AC_ADD_FAQ } from '../actions/faqs';
import swal from 'sweetalert';

class addFaq extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: '',
      answerError: false,
      answerCountError: false,
      color0: '',
      question: '',
      questionError: false,
      questionCountError: false,
      color1: '',
      status: '',
      statusError: false,
      color2: '',

    }
    this.validation = this.validation.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

  }

  validation() {
    const question = this.state.question;
    const answer = this.state.answer;
    const status = this.state.status
    if (question) {
      if (question.length < 5) {
        this.setState({ questionError: false, questionCountError: true, color0: "red" })
      }
      else {
        this.setState({ questionError: false, questionCountError: false, color0: "" })
      }
    }
    else {
      this.setState({ questionError: true, questionCountError: false, color0: "red" })
    }

    if (answer) {
      if (answer.length < 5) {
        this.setState({ answerError: false, answerCountError: true, color1: "red" })
      }
      else {
        this.setState({ answerError: false, answerCountError: false, color1: "" })
      }
    }
    else {
      this.setState({ answerError: true, answerCountError: false, color1: "red" })
    }

    if (status) {
      this.setState({ statusError: false, color2: '' })
    }
    else {
      this.setState({ statusError: true, color2: '1px solid red' })
    }
    if (question && answer && status) {
      document.getElementById('addFaq').reset();
      swal("Faq Added Successfully!", {
        buttons: false,
        timer: 2000,
      });
      this.setState({ question: '', answer: '', status: '' });
    }


    const formData = {
      question: this.state.question,
      answer: this.state.answer,
      status: this.state.status
    }
    this.props.AC_ADD_FAQ(formData);
    console.log('-=value-=', formData)

  }
  handleInputChange(event) {
    const fieldId = event.target.id;
    const fieldValue = event.target.value;

    if (fieldId === "question") {
      this.setState({ question: fieldValue })
      if (fieldValue) {
        if (fieldValue.length < 5) {
          this.setState({ questionError: false, questionCountError: true, color0: 'red' })
        }
        else {
          this.setState({ questionError: false, questionCountError: false, color0: '' })
        }
      }
      else {
        this.setState({ questionError: true, questionCountError: false, color0: '' })
      }
    }

    if (fieldId === "answer") {
      this.setState({ answer: fieldValue })
      if (fieldValue) {
        if (fieldValue.length < 5) {
          this.setState({ answerError: false, answerCountError: true, color1: 'red' })
        }
        else {
          this.setState({ answerError: false, answerCountError: false, color1: '' })
        }
      }
      else {
        this.setState({ answerError: true, answerCountError: false, color1: '' })
      }
    }

    if (fieldId === "status") {
      this.setState({ status: fieldValue })
      if (fieldValue) {
        this.setState({ statusError: false, color2: '' })
      }
      else {
        this.setState({ statusError: true, color2: '1px solid red' })
      }
    }
  }


  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Add Faq</h4>
                <form className="forms-sample" id="addFaq">
                  <div className="form-group">
                    <label for="exampleInputUsername1">QUESTION</label>
                    <input type="text" autoComplete='off' placeholder="Question" id="question" value={this.state.question} onChange={this.handleInputChange} style={{ borderColor: this.state.color0 }} className="form-control" />
                    {this.state.questionError ? <label className="mt-2" style={{ color: 'red' }}>Question is required</label> : ""}
                    {this.state.questionCountError ? <label className="mt-2" style={{ color: 'red' }}>Question should be atleast 5 characters</label> : ""}

                  </div>
                  <div className="form-group">
                    <label for="exampleInputUsername1">ANSWER</label>
                    <input type="text" autoComplete='off' placeholder="Answer" id="answer" value={this.state.answer} onChange={this.handleInputChange} style={{ borderColor: this.state.color1 }} className="form-control" />
                    {this.state.answerError ? <label className="mt-2" style={{ color: 'red' }}>Answer is required</label> : ""}
                    {this.state.answerCountError ? <label className="mt-2" style={{ color: 'red' }}>Answer should be atleast 5 characters</label> : ""}

                  </div>
                  <div className="form-group" >
                    <label for="exampleInputUsername1">STATUS</label>
                    <select className="form-control" id="status" style={{ backgroundColor: 'white' }} onChange={this.handleInputChange} >
                      <option value="Status">Status</option>
                      <option value="active">Active</option>
                      <option value="inactive">Inactive</option>
                    </select>
                    {this.state.statusError ? <label className="mt-2" style={{ color: 'red' }}>Status is required</label> : ""}
                  </div>
                  <button type="button" className="btn btn-gradient-primary me-2" style={{
                    backgroundColor: 'blue',
                    color: 'white'
                  }} onClick={this.validation}>Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log('map state', state);
  return {
    faqsReducer: state.faqsReducer
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ AC_LIST_FAQS, AC_ADD_FAQ }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(addFaq);
