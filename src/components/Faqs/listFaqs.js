import React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_LIST_FAQS, AC_ADD_FAQ, AC_DELETE_FAQ } from '../actions/faqs';
import swal from 'sweetalert';
import { Redirect} from "react-router-dom";

class listFaqs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editStatus: false,
            viewStatus: false
        }
        this.deleteFaq = this.deleteFaq.bind(this);
        this.editFaq = this.editFaq.bind(this);
        this.viewFaq = this.viewFaq.bind(this);
    }
    componentDidMount() {
        this.props.AC_LIST_FAQS();
        this.props.AC_ADD_FAQ();
        this.props.AC_DELETE_FAQ();

    }

    editFaq(event) {
        let faqId = event.target.id;
        this.setState({ editStatus: true, editId: faqId })
    }
    viewFaq(event) {
        let faqId = event.target.id;
        this.setState({ viewStatus: true, viewId: faqId })
    }

    deleteFaq(event) {
        let faqId = event.target.id;
        var formData = {
            id: faqId,
        }
        swal({
            title: "Are you sure?",
            text: "Do you want to Delete the Faq!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.AC_LIST_FAQS();
                    this.props.AC_DELETE_FAQ(formData);
                    this.props.AC_LIST_FAQS();
                    swal("Faq Deleted Successfully!", {
                        buttons: false,
                        timer: 2000,
                    });
                } 
            });
    }

    render() {
        if (this.state.editStatus) {
            return <Redirect to={'/editFaq'+'/'+this.state.editId} />
        }
        if (this.state.viewStatus) {
            return <Redirect to={'/viewFaq'+'/'+this.state.viewId} />
        }

        var listFaq = this.props.faqsReducer.listFaqs;
        var activeCount = 0;
        var inactiveCount = 0;
        var resultArray = [];
        if (listFaq != undefined) {
            if (listFaq.length > 0) {
                for (var i = 0; i < listFaq.length; i++) {
                    if (listFaq[i].status == "active") {
                        activeCount++;
                    }
                    else {
                        inactiveCount++;
                    }
                    resultArray.push(
                        <tr key={listFaq[i]._id}>
                            <th scope="row">{i + 1}</th>
                            <td>{listFaq[i].question}</td>
                            <td>{listFaq[i].answer}</td>
                            <td>{listFaq[i].status}</td>
                            <td>
                                <i class="menu-icon mdi mdi-tooltip-edit text-primary me-2" id={listFaq[i]._id} onClick={this.editFaq} style={{fontSize: "22px"}}></i>
                                <i class="menu-icon mdi mdi-view-list text-primary me-2" id={listFaq[i]._id} onClick={this.viewFaq} style={{fontSize: "25px" }}></i>
                                <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={listFaq[i]._id} onClick={this.deleteFaq} style={{fontSize: "25px" }}></i>
                            </td>
                        </tr>
                    )
                }
            }
        }



        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total FAQS</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{listFaq.length}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{activeCount}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{inactiveCount}</h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{ marginBottom: "275px" }}>
                    <thead>
                        <h4 className="card-title"><b>List Faqs</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Question</th>
                            <th scope="col">Answer</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>
            </>

        );
    }
}



function mapStateToProps(state) {
    console.log('map state', state);
    return {
        faqsReducer: state.faqsReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_FAQ, AC_LIST_FAQS, AC_DELETE_FAQ }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(listFaqs);