import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_LIST_FAQS, AC_ADD_FAQ, AC_VIEW_FAQ, AC_HANDLE_INPUT_CHANGE } from '../actions/faqs';
import { Redirect} from "react-router-dom";


class viewFaq extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            viewStatus: false
        }
        this.back = this.back.bind(this);
    }
    back(){
       this.setState({viewStatus:true});
    }
    componentWillMount() {
        let faqId = this.props.match.params.id;
        let formData = {id:faqId}
        this.props.AC_VIEW_FAQ(formData);
    }

    render() {

        const question = this.props.faqsReducer.faqInfo.question;
        const answer = this.props.faqsReducer.faqInfo.answer;
        const status = this.props.faqsReducer.faqInfo.status;
        if(this.state.viewStatus){
           return <Redirect to='/listFaqs' />
        }
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">View Faq</h4>
                                <form className="forms-sample" id="editFaq">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">QUESTION</label>
                                        <input type="text" autoComplete='off' placeholder="Question" id="question" value={question} className="form-control" disabled/>
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">ANSWER</label>
                                        <input type="text" autoComplete='off' placeholder="Answer" id="answer" value={answer}className="form-control" disabled/>
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">STATUS</label>
                                        <select className="form-control"style={{ backgroundColor: 'white' }} value={status} id="status"disabled >
                                            <option value="">Select Status</option>
                                            <option value="active" selected={status == true}>Active</option>
                                            <option value="inactive" selected={status == false}>Inactive</option>
                                        </select>
                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{
                                        backgroundColor: 'blue',
                                        color: 'white'
                                    }} onClick={this.back}>Back</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log('map state', state);
    return {
        faqsReducer: state.faqsReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_LIST_FAQS, AC_ADD_FAQ, AC_VIEW_FAQ,AC_HANDLE_INPUT_CHANGE }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(viewFaq);
