import React from 'react';
import { AC_ADD_IMAGE, AC_LIST_IMAGES } from "../actions/category";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import Default from "../../images/default image.png";


class addImage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            category: '',
            categoryError: false,
            categoryCountError: false,
            color0: '',
            file: {},
            fileError: '',
            color1: "",
            status: '',
            statusError: false,
            color2: '',
            imagedefault: Default,
        }
        this.validation = this.validation.bind(this)
        this.inputchange = this.inputchange.bind(this);

    }
    componentDidMount() {
        this.props.AC_LIST_IMAGES();
    }
    validation() {
        const category = this.state.category;
        const filedata = this.state.file;
        const status = this.state.status

        if (category) {
            if (category.length < 5) {
                this.setState({ categoryError: false, categoryCountError: true, color0: "red" })
            }
            else {
                this.setState({ categoryError: false, categoryCountError: false, color0: "" })
            }
        }
        else {
            this.setState({ categoryError: true, categoryCountError: false, color0: "red" })
        }
        if (filedata.name) {
            this.setState({ fileError: false })
        }
        else {
            this.setState({ fileError: true })
        }
        console.log("--=-=filedata", filedata.name)


        if (status) {
            this.setState({ statusError: false, color2: '' })
        }
        else {
            this.setState({ statusError: true, color2: '1px solid red' })
        }

        if (category && filedata && status) {
            document.getElementById('addImage').reset();
            swal("Image Added Successfully!", {
                buttons: false,
                timer: 2000,
            });
            this.setState({ category: '', filedata: '', status: '' });
        }
        var formdata = new FormData();
        formdata.append("category", category);
        formdata.append('image', filedata);
        formdata.append("status", status);
        this.props.AC_LIST_IMAGES();
        this.props.AC_ADD_IMAGE(formdata);
        this.props.AC_LIST_IMAGES();
    }


    inputchange(event) {
        const fileid = event.target.id;
        const fieldId = event.target.id;
        const fieldValue = event.target.value;

        if (fieldId == "category") {
            this.setState({ category: fieldValue })
            if (fieldValue) {
                if (fieldValue.length < 5) {
                    this.setState({ categoryError: false, categoryCountError: true, color0: 'red' })
                }
                else {
                    this.setState({ categoryError: false, categoryCountError: false, color0: '' })
                }
            }
            else {
                this.setState({ categoryError: true, categoryCountError: false, color0: '' })
            }
        }
        if (fileid === "upload") {
            let file = event.target.files[0];
            this.setState({ file: file, fileError: false })
        }
        var file = this.refs.file.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        reader.onloadend = function (e) {
            this.setState({
                imgSrc: [reader.result]
            })
        }.bind(this);
        console.log("url", url)

        if (fieldId === "status") {
            this.setState({ status: fieldValue })
            if (fieldValue) {
                this.setState({ statusError: false, color2: '' })
            }
            else {
                this.setState({ statusError: true, color2: '1px solid red' })
            }
        }

    }
    render() {
        return (
            <>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-6 grid-margin stretch-card">
                            <div className="card">
                                <div className="card-body">
                                    <h4 className="card-title">Add Image</h4>
                                    <form className="forms-sample" id="addImage">
                                        <div className="form-group">
                                            <label for="exampleInputUsername1">CATEGORY</label>
                                            <input type="text" autoComplete='off' placeholder="Category" id="category" value={this.state.category} onChange={this.inputchange} style={{ borderColor: this.state.color0 }} className="form-control" />
                                            {this.state.categoryError ? <label className="mt-2" style={{ color: 'red' }}>Categroy is required</label> : ""}
                                            {this.state.categoryCountError ? <label className="mt-2" style={{ color: 'red' }}>Categroy should be atleast 5 characters</label> : ""}
                                        </div>
                                        <div className="form-group">
                                            <label for="exampleInputUsername1">IMAGE</label>
                                            <img src={this.state.imgSrc} style={{width:"100px",height:"150px"}} class=" round rounded ml-auto d-block" alt="" />
                                            <br />
                                            <input type="file" id="upload" ref="file" name="user[image]" multiple="true" onChange={this.inputchange} style={{ borderColor: this.state.color1, }}></input>
                                            <br />

                                            {this.state.fileError ? <label className="mt-2" style={{ color: 'red' }}>Image is required</label> : ""}
                                        </div>

                                        <div className="form-group" >
                                            <label for="exampleInputUsername1">STATUS</label>
                                            <select className="form-control" id="status" style={{ backgroundColor: 'white' }} onChange={this.inputchange} style={{ borderColor: this.state.color0 }} >
                                                <option value="Status">Status</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                            {this.state.statusError ? <label className="mt-2" style={{ color: 'red' }}>Status is required</label> : ""}
                                        </div>
                                        <button type="button" className="btn btn-gradient-primary me-2" style={{
                                            backgroundColor: 'blue',
                                            color: 'white'
                                        }} onClick={this.validation}>Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function mapStateToProps(state) {

    return {

        image: state.imagesReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_IMAGE, AC_LIST_IMAGES }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(addImage);



