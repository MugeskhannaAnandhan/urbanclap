import React from 'react';
import { Link } from 'react-router-dom';
import { AC_ADD_IMAGE, AC_LIST_IMAGES, AC_DELETE_IMAGE } from "../actions/category";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Swal from 'sweetalert';
import { Redirect } from 'react-router-dom'

class listImages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listImage: true,
            viewStatus: false
        }
        this.deleteInput = this.deleteInput.bind(this)
        this.deleteImageOperation = this.deleteImageOperation.bind(this)
        this.viewImage = this.viewImage.bind(this);

    }
    componentWillMount() {
        this.props.AC_LIST_IMAGES();
    }
    viewImage(event) {
        let imageId = event.target.id;
        this.setState({ viewStatus: true, viewId: imageId })
    }
    deleteInput(event) {
        var datalistId = event.target.id;
        var deleteImageid = event.target.id;
        Swal({
            title: "Are you sure?",
            text: "Do you want to Delete the Faq!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.deleteImageOperation(datalistId);
                    Swal('Image Deleted Successfully!', 'You clicked button!', 'success');

                } else {
                    Swal('Image not deleted!',);
                }
            });
    }
    deleteImageOperation(datalistId) {
        var userData = {
            id: datalistId
        }

        this.props.AC_LIST_IMAGES();
        this.props.AC_DELETE_IMAGE(userData);
        this.props.AC_LIST_IMAGES();
        setTimeout(
            () => this.setState({ listImage: true }),
            3000
        );
    }

    render() {
        if (this.state.viewStatus) {
            return <Redirect to={'/viewImage'+'/'+this.state.viewId} />
        }
        var datalist = this.props.imagesReducer.listImages;
        console.log("----datalist--", datalist);
        var activecount = 0;
        var inactivecount = 0;
        var totalcount = datalist.length;
        var resultArray = [];
        if (datalist) {
            for (var i = 0; i < datalist.length; i++) {
                if (datalist[i].status == "active") {
                    activecount++;
                }
                else {
                    inactivecount++;
                }
                resultArray.push(<tr key={i} >
                    <td>{i + 1}</td>
                    <td>{datalist[i].category}</td>
                    <td ><img className='="img-thumbnail' src={"http://localhost:8000/uploads/" + datalist[i].filename}></img></td>
                    <td>{datalist[i].status}</td>
                    <td>
                        <i class="menu-icon mdi mdi-view-list text-primary me-2"  id={datalist[i]._id} onClick={this.viewImage} style={{ fontSize: "25px" }}></i>
                        <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={datalist[i]._id} onClick={this.deleteInput}  style={{ fontSize: "25px" }}></i>
                    </td>
                </tr>)
            }
        }

        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total Images</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>
                                                            {totalcount}
                                                        </h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>
                                                            {activecount}
                                                        </h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>
                                                            {inactivecount}
                                                        </h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{ marginBottom: "275px" }}>
                    <thead>
                        <h4 className="card-title"><b>List Images</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Category</th>
                            <th scope="col">Image</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>

            </>
        )
    }
}

function mapStateToProps(state) {

    return {

        imagesReducer: state.imagesReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_IMAGE, AC_LIST_IMAGES, AC_DELETE_IMAGE }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(listImages);

