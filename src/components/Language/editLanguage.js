import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_LANGUAGE, AC_LIST_LANGUAGES, AC_VIEW_LANGUAGE,AC_HANDLE_INPUT_CHANGE } from '../actions/language';
import { Redirect } from 'react-router-dom';

class editLanguage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            codeError: false,
            name: '',
            nameError: false,
            status: '',
            statusError: false,
            editStatus: false

        }
        this.validation = this.validation.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.back = this.back.bind(this);

    }
    back(){
        this.setState({editStatus:true});
    }

    validation() {
        const name = this.props.languagesReducer.languageInfo.name;
        const code = this.props.languagesReducer.languageInfo.code;
        const status = this.props.languagesReducer.languageInfo.status;
        const id = this.props.languagesReducer.languageInfo.id;
        let formData = {
            name : name,
            code : code,
            status : status,
            id : id
        }
        this.props.AC_ADD_LANGUAGE(formData);

    }
    handleInputChange(event) {
        let name = event.target.id;
        let value = event.target.value;
        this.props.AC_HANDLE_INPUT_CHANGE(name,value);
    }

    componentWillMount() {
        let countryId = this.props.match.params.id;
        let formData = {id: countryId}
        this.props.AC_VIEW_LANGUAGE(formData);
    }

    render() {
        const name = this.props.languagesReducer.languageInfo.name;
        const code = this.props.languagesReducer.languageInfo.code;
        const status = this.props.languagesReducer.languageInfo.status;
        if(this.state.editStatus){
            return <Redirect to ='/listLanguages'/>
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Edit Language</h4>
                                <form className="forms-sample" id="editLanguage">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">NAME</label>
                                        <input type="text" autoComplete='off' placeholder="Name" id="name" value={name} onChange={this.handleInputChange} style={{ borderColor: this.state.color0 }} className="form-control" />
                                        {this.state.nameError ? <label className="mt-2" style={{ color: 'red' }}>Name is required</label> : ""}
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">CODE</label>
                                        <input type="text" autoComplete='off' placeholder="Code" id="code" value={code} onChange={this.handleInputChange} style={{ borderColor: this.state.color1 }} className="form-control" />
                                        {this.state.codeError ? <label className="mt-2" style={{ color: 'red' }}>Code is required</label> : ""}
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">STATUS</label>
                                        <select className="form-control" id="status"value={status} style={{ backgroundColor: 'white' }} onChange={this.handleInputChange} >
                                            <option value="Status">Select Status</option>
                                            <option value="active" selected ={status ==true} >Active</option>
                                            <option value="inactive" selected={status == false}>Inactive</option>
                                        </select>
                                        {this.state.statusError ? <label className="mt-2" style={{ color: 'red' }}>Status is required</label> : ""}
                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{ backgroundColor: 'blue',color: 'white'}} onClick={this.validation}>Submit</button>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{ backgroundColor: 'blue',color: 'white', width:"100px"}} onClick={this.back}>Back</button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        languagesReducer: state.languagesReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_LANGUAGE, AC_LIST_LANGUAGES, AC_VIEW_LANGUAGE, AC_HANDLE_INPUT_CHANGE }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(editLanguage);
