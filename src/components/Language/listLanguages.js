import React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_LANGUAGE, AC_LIST_LANGUAGES,AC_DELETE_LANGUAGE } from '../actions/language';
import swal from 'sweetalert';
import { Redirect} from "react-router-dom";

class listLanguages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editStatus: false,
            viewStatus: false
        }
        this.deleteLanguage = this.deleteLanguage.bind(this);
        this.editLanguage = this.editLanguage.bind(this);
        this.viewLanguage = this.viewLanguage.bind(this);
    }
    componentDidMount() {
        this.props.AC_LIST_LANGUAGES();
        this.props.AC_ADD_LANGUAGE();
        this.props.AC_DELETE_LANGUAGE();

    }

    editLanguage(event){
        let languageId = event.target.id;
        this.setState({ editStatus: true, editId: languageId })
    }
    viewLanguage(event){
        let languageId = event.target.id;
        this.setState({ viewStatus: true, viewId: languageId })
    }
    deleteLanguage(event) {
        let languageId = event.target.id;
        var formData = {
            id: languageId,
        }
        swal({
            title: "Are you sure?",
            text: "Do you want to Delete the Language!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.AC_LIST_LANGUAGES();
                    this.props.AC_DELETE_LANGUAGE(formData);
                    this.props.AC_LIST_LANGUAGES();
                    swal("Language Deleted Successfully!", {
                        buttons: false,
                        timer: 2000,
                      });
                } 
            });
    }

    render() {
        if (this.state.editStatus) {
            return <Redirect to={'/editLanguage'+'/'+this.state.editId} />
        }
        if (this.state.viewStatus) {
            return <Redirect to={'/viewLanguage'+'/'+this.state.viewId} />
        }

        var listLanguage = this.props.languagesReducer.listLanguages;
        var activeCount = 0;
        var inactiveCount = 0;
        var resultArray = [];
        if (listLanguage !== undefined) {
            if (listLanguage.length > 0) {
                for (var i = 0; i < listLanguage.length; i++) {
                    if (listLanguage[i].status == "active") {
                        activeCount++;
                    }
                    else {
                        inactiveCount++;
                    }
                    resultArray.push(
                        <tr key={listLanguage[i]._id}>
                            <th scope="row">{i + 1}</th>
                            <td>{listLanguage[i].name}</td>
                            <td>{listLanguage[i].code}</td>
                            <td>{listLanguage[i].status}</td>
                            <td>
                                <i class="menu-icon mdi mdi-tooltip-edit text-primary me-2" id={listLanguage[i]._id} onClick={this.editLanguage} style={{fontSize: "22px"}}></i>
                                <i class="menu-icon mdi mdi mdi-view-list text-primary me-2" id={listLanguage[i]._id} onClick={this.viewLanguage} style={{fontSize: "25px" }}></i>
                                <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={listLanguage[i]._id} onClick={this.deleteLanguage} style={{fontSize: "25px" }}></i>
                            </td>
                        </tr>
                    )
                }
            }
        }

        

        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total Languages</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{listLanguage.length}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage">{activeCount}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage">{inactiveCount}</h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{marginBottom: "275px"}}>
                    <thead>
                    <h4 className="card-title"><b>List Languages</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Code</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>
            </>

        );
    }
}



function mapStateToProps(state) {
    console.log('map state', state);
    return {
        languagesReducer: state.languagesReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_LANGUAGE, AC_LIST_LANGUAGES,AC_DELETE_LANGUAGE }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(listLanguages);