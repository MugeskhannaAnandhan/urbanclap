import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { AC_ADD_PAGE, AC_LIST_PAGES } from '../actions/pages';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";


class addPage extends Component {
    constructor(props) {
        super(props);
        const html = '<p></p>';
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                title: "",
                description: "",
                slug: "",
                titleError: false,
                descriptionError: false,
                slugError: false,
                editorState,

            }
        }

        this.edit = this.edit.bind(this);
        this.validation = this.validation.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onEditorStateChange = this.onEditorStateChange.bind(this);


    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState, descriptionError: false
        });
        console.log("-=-=-=-editor-=-=-", editorState);
    }

    validation() {
        var title = this.state.title;
        var slug = this.state.slug;
        var listPages = this.props.pagesReducer.listPages;
        const editorState = this.state.editorState;
        let value = draftToHtml(convertToRaw(editorState.getCurrentContent()))
        let data = convertToRaw(editorState.getCurrentContent());
        let description = data.blocks[0].text;

        if (title) {
            this.setState({ titleError: false })
        }
        else {
            this.setState({ titleError: true })
        }
        if (slug) {
            this.setState({ slugError: false })
        }
        else {
            this.setState({ slugError: true })
        }
        if (description) {
            this.setState({ descriptionError: false })
        }
        else {
            this.setState({ descriptionError: true })
        }


        if (title && slug && description) {
            var formData = {
                title: title,
                description: value,
                slug: slug,
            }

            this.props.AC_ADD_PAGE(formData);
        }
        if (title && slug && description) {
            document.getElementById('addPage').reset();
            swal("Page Added Successfully!", {
                buttons: false,
                timer: 2000,
            });
            this.setState({ title: '', slug: '', description1: '' });


        }
        
    }


    handleInputChange(event) {
        var name = event.target.id;
        var value = event.target.value;

        if (name == "title") {
            if (value) {

                this.setState({ title: value, titleError: false })
            }
            else {
                this.setState({ title: value, titleError: true })
            }
        }

        if (name == "slug") {
            if (value) {

                this.setState({ slug: value, slugError: false })
            }
            else {
                this.setState({ slug: value, slugError: true })
            }
        }
    }
    componentDidMount() {
        this.props.AC_LIST_PAGES();
    }


    edit() {
        var pageValue = this.state.editorState.getCurrentContent().getPlainText();
        if (pageValue) {

            this.setState({ description: pageValue, descriptionError: false })
        }
        else {
            this.setState({ description: pageValue, descriptionError: true })
        }

        console.log("=-=-=description-=-=-", this.state.description);
    }


    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">Add Page</h4>
                                <form className="forms-sample" id="addPage">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">TITLE</label>
                                        <input type="text" className="form-control" id="title" placeholder="Enter Your Title" onChange={this.handleInputChange} />
                                        {this.state.titleError ? <label style={{ color: "red" }}>Title is required</label> : ""}


                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">SLUG</label>
                                        <input type="text" className="form-control" id="slug" placeholder="Enter Your Slug" onChange={this.handleInputChange} />
                                        {this.state.slugError ? <label style={{ color: "red" }}>Slug is Required</label> : ""}
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputPassword1" >DESCRIPTION</label>
                                        <Editor toolbarClassName="toolbarClassName" wrapperClassName="wrapperClassName" onEditorStateChange={this.onEditorStateChange} editorState={this.state.editorState} editorClassName="editorClassName" placeholder="What ever you mention goes here..." onChange={this.edit} />
                                        {this.state.descriptionError ? <label style={{ color: "red" }}>Description is Required</label> : ""}
                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{
                                        backgroundColor: 'blue',
                                        color: 'white'
                                    }} onClick={this.validation}>Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


function mapStateToProps(state) {
    return {
        pagesReducer: state.pagesReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_PAGE, AC_LIST_PAGES }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(addPage);






