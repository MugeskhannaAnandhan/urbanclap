import React from "react";
import { AC_ADD_PAGE, AC_LIST_PAGES, AC_VIEW_PAGE, AC_HANDLE_INPUT_CHANGE } from "../actions/pages";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Editor, } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import "../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import htmlToDraft from 'html-to-draftjs';
import { Redirect } from 'react-router-dom';

class editPage extends React.Component {
    constructor(props) {
        super(props);
        const html = '<p></p>';
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                title: '',
                titleError: false,
                description: '',
                descriptionError: false,
                editStatus:false,
                editorState,

            }
            this.validation = this.validation.bind(this)
            this.inputchange = this.inputchange.bind(this);
            this.edit = this.edit.bind(this);
            this.onEditorStateChange = this.onEditorStateChange.bind(this);
            this.back = this.back.bind(this);
        }
    }
    back(){
        this.setState({editStatus:true})
    }
    onEditorStateChange(editorState) {
        this.setState({
            editorState,
        });
    };
    edit() {
        var pagevalue = this.state.editorState.getCurrentContent().getPlainText();
        if (pagevalue) {

            this.setState({ description: pagevalue, descriptionError: false })
        }
        else {
            this.setState({ description: pagevalue, descriptionError: true })
        }
        console.log("=-=-=description-=-=-", this.state.description);
    }

    validation() {
        const title = this.props.pagesReducer.pageInfo.title;
        const slug = this.props.pagesReducer.pageInfo.slug;
        // const description = this.props.page.viewPage.description;
        const id = this.props.pagesReducer.pageInfo.id;
        const description = this.state.description;
        let formData = {
            title: title,
            description: description,
            id: id,
            slug: slug
        }
        this.props.AC_ADD_PAGE(formData)
        console.log("-=-=getting", formData)
    }
    inputchange(event) {
        let name = event.target.id;
        let value = event.target.value
        this.props.AC_HANDLE_INPUT_CHANGE(name, value)

    }

    componentWillMount() {
        let pageId = this.props.match.params.id;
        var formData = { id: pageId }
        this.props.AC_VIEW_PAGE(formData)
        setTimeout(() => {
            this.editorContent();
        }, 500);
    }
    editorContent() {
        const description = this.props.pagesReducer.pageInfo.description;
        const html = '<p>' + description + '</p>';
        const contentBlock = htmlToDraft(html);
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        this.setState({ editorState });

    }
    render() {
        const title = this.props.pagesReducer.pageInfo.title;
        const slug = this.props.pagesReducer.pageInfo.slug;
        const description = this.props.pagesReducer.pageInfo.description;
        const status = this.props.pagesReducer.pageInfo.status;
        const { editorState } = this.state;
        if(this.state.editStatus){
            return <Redirect to = '/listPages'/>
        }
        return (
            <>
                <h4 className="card-title">Edit Page</h4>
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <form class="forms-sample">
                                <div class="form-group">
                                    <label for="exampleInputUsername1">TITLE</label>
                                    <input type="text" placeholder="Title" id="title" value={title} onChange={this.inputchange} class="form-control" />
                                </div>

                               
                                <div className="form-group">
                                    <label for="exampleInputUsername1">SLUG</label>
                                    <input type="text" className="form-control" id="slug" value={slug} placeholder="Enter Your Slug" onChange={this.inputchange} />
                                </div>
                                <div class="form-group">
                                    <h4 >Description</h4>
                                    <Editor id="description"
                                        toolbarClassName="toolbarClassName"
                                        wrapperClassName="wrapperClassName"
                                        onEditorStateChange={this.onEditorStateChange}
                                        editorState={this.state.editorState}
                                        editorClassName="editorClassName"
                                        onChange={this.edit} placeholder="Enter the Description" />
                                </div>
                                
                                <button type="button" class="btn btn-gradient-primary me-2" onClick={this.validation} style={{backgroundColor:"blue", color:"white"}}>Submit</button>
                                <button type="button" class="btn btn-gradient-primary me-2" onClick={this.back} style={{backgroundColor:"blue", color:"white",width:'100px'}}>Back</button>

                            </form>

                        </div>
                    </div>
                </div>
            </>
        )
    }
}

function mapStateToProps(state) {
    console.log("-=-=-=,", state);
    return {

        pagesReducer: state.pagesReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_PAGE, AC_LIST_PAGES, AC_VIEW_PAGE, AC_HANDLE_INPUT_CHANGE }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(editPage);
