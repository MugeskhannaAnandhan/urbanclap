import React from "react";
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { bindActionCreators } from 'redux';
import { AC_LIST_PAGES, AC_DELETE_PAGE, AC_ADD_PAGE,AC_VIEW_PAGE } from '../actions/pages';
import { Redirect } from 'react-router-dom';
import Parser from "html-react-parser";

class listPages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editStatus: false,
            editId: '',
            viewStatus: false,
            viewId: '',
            id: ''
        }

        this.deletePage = this.deletePage.bind(this);
        this.editPage = this.editPage.bind(this);
        this.viewPage = this.viewPage.bind(this);

    }
    componentDidMount() {
        this.props.AC_LIST_PAGES();
        this.props.AC_ADD_PAGE();
        this.props.AC_DELETE_PAGE();
    }

    editPage(event) {
        let pageId = event.target.id;
        this.setState({ editStatus: true, editId: pageId })
    }
    viewPage(event) {
        let pageId = event.target.id;
        this.setState({ viewStatus: true, viewId: pageId })
    }


    deletePage(event) {
        let pageId = event.target.id;
        var formData = {
            id: pageId,
        }
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.AC_LIST_PAGES();
                    this.props.AC_DELETE_PAGE(formData);
                    this.props.AC_LIST_PAGES();
                    swal("Poof! Your file has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Done", "Your file is safe!", "success");
                }
            });
    }
   


    render() {
        if (this.state.editStatus) {
            return <Redirect to={'/editPage' + '/' + this.state.editId} />

        }
        if (this.state.viewStatus) {
            return <Redirect to={'/viewPage' + '/' + this.state.viewId} />
        }
        var listPage = this.props.pagesReducer.listPages;
        var description =this.props.pagesReducer.listPages.description;
        var activeCount = 0;
        var inactiveCount = 0;
        var resultArray = [];
        if(description){
            return description.substring(1,20).replace('.....');
        }
        if (listPage !== undefined) {
            if (listPage.length > 0) {
                for (var i = 0; i < listPage.length; i++) {
                    if (listPage[i].status == "active") {
                        activeCount++;
                    }
                    else {
                        inactiveCount++;
                    }
                    resultArray.push(
                        <tr key={listPage[i]._id}>
                            <th scope="row">{i + 1}</th>
                            <td>{listPage[i].title}</td>
                            <td>{listPage[i].slug}</td>
                            <td><p className='description' >{Parser(listPage[i].description)}</p></td>
                            <td>
                                <i class="menu-icon mdi mdi-tooltip-edit text-primary me-2" id={listPage[i]._id} onClick={this.editPage} style={{fontSize: "22px"}}></i>
                                <i class="menu-icon mdi mdi-view-list text-primary me-2" id={listPage[i]._id} onClick={this.viewPage} style={{fontSize: "25px" }}></i>
                                <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={listPage[i]._id} onClick={this.deletePage} style={{fontSize: "25px" }}></i>
                                
                            </td>
                        </tr>
                    )
                }
            }
        }
        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total PAGES</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{listPage.length}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{activeCount}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{inactiveCount}</h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{ marginBottom: "275px" }}>
                    <thead>
                        <h4 className="card-title"><b>List Pages</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Description</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>
            </>
        );
    }
}




function mapStateToProps(state) {
    console.log("=-=-delete", state);
    return {
        pagesReducer: state.pagesReducer
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({AC_ADD_PAGE, AC_LIST_PAGES, AC_DELETE_PAGE, AC_VIEW_PAGE }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(listPages);