import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { AC_ADD_PAGE, AC_LIST_PAGES, AC_VIEW_PAGE, AC_HANDLE_INPUT_CHANGE } from '../actions/pages';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Parser from 'html-react-parser';
import { Redirect } from 'react-router-dom';


class viewPage extends Component {
    constructor(props) {
        super(props);
        var html = '<p></p>';
        var contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                title: "",
                description: "",
                slug: "",
                editorState,
                viewStatus:false

            }
        }
        this.edit = this.edit.bind(this);
        this.validation = this.validation.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.editorContent = this.editorContent.bind(this);
        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.back = this.back.bind(this);


    }
    back(){
        this.setState({viewStatus:true})
    }

    onEditorStateChange(editorState) {
        const name = "description"
        this.setState({
            editorState,
        });
        const value = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        const data  = convertToRaw(editorState.getCurrentContent());
        const description = data.blocks[0].text;
        this.props.AC_HANDLE_INPUT_CHANGE(name, value);
    };


    validation() {
        var title = this.props.pagesReducer.pageInfo.title;
        var description = this.props.pagesReducer.pageInfo.description;
        var slug = this.props.pagesReducer.pageInfo.slug;
        let pageId = this.props.match.params.id;

        let formData = {
            title: title,
            description: description,
            slug: slug,
            id: pageId
        }
        this.props.AC_ADD_PAGE(formData);
        swal("Good job!", "Page Updated Successfully!", "success");
        
    }

    handleInputChange(event) {
        var name = event.target.id;
        var value = event.target.value;
        if (name === 'title') {
            this.setState({ title: value });
            this.props.AC_HANDLE_INPUT_CHANGE(name, value);
            var title = value;
            if (title) {
                if (title.length >= 5) {
                    this.setState({ titleError: false })
                }
                else {
                    this.setState({titleError: false })
                }
            }
            else {
                this.setState({ titleError: true});
            }
        }

    }

    editorContent()
    {
        var pageData = this.props.pagesReducer.pageInfo;
        if(pageData!==undefined){
        var description = pageData.description;
        var html = '<p>'+description+'</p>';
        var contentBlock = htmlToDraft(html)
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        this.setState({editorState});
        }
    }

    componentWillMount() {
        let pageId = this.props.match.params.id;
        let formData = {id:pageId}
        this.props.AC_VIEW_PAGE(formData);
    }


    edit() {
        var pageValue = this.state.editorState.getCurrentContent().getPlainText();
        if (pageValue) {

            this.setState({ description: pageValue, descriptionError: false })
        }
        else {
            this.setState({ description: pageValue, descriptionError: true })
        }

        console.log("=-=-=description-=-=-", this.state.description);
    }



    render() {
        var pageData = this.props.pagesReducer.pageInfo;
        if (pageData !== undefined) {
            var title = pageData.title;
            var description = Parser(pageData.description);
            var slug = pageData.slug;
        }
        if (this.state.viewStatus) {
            return <Redirect to="/listPages" />
        }

        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-6 grid-margin stretch-card">
                        <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">View Page</h4>
                                <form className="forms-sample" id="addPage">
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">TITLE</label>
                                        <input type="text" className="form-control" id="title" value={title} placeholder="Enter Your Title" onChange={this.handleInputChange} />
                                        {this.state.titleError ? <label style={{ color: "red" }}>Title is required</label> : ""}


                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputUsername1">SLUG</label>
                                        <input type="text" className="form-control" id="slug" value={slug} placeholder="Enter Your Slug" onChange={this.handleInputChange} />
                                        {this.state.slugError ? <label style={{ color: "red" }}>Slug is Required</label> : ""}
                                    </div>
                                    <div className="form-group">
                                        <label for="exampleInputPassword1" >DESCRIPTION</label>
                                        {description}

                                    </div>
                                    <button type="button" className="btn btn-gradient-primary me-2" style={{
                                        backgroundColor: 'blue',
                                        color: 'white'
                                    }} onClick={this.back}>Back</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}


function mapStateToProps(state) {
    return {
        pagesReducer: state.pagesReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_PAGE, AC_LIST_PAGES, AC_VIEW_PAGE, AC_HANDLE_INPUT_CHANGE }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(viewPage);






