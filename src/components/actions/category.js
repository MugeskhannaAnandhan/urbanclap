import axios from "axios";
import URL from "../../common/api";
const ADD_IMAGE = "ADD_IMAGE";
const LIST_IMAGES = "LIST_IMAGES";
const DELETE_IMAGE = "DELETE_IMAGE";
const VIEW_IMAGE = "VIEW_IMAGE";
const EDIT_IMAGE="EDIT_IMAGE";
const UPDATE_IMAGE_DATA = "UPDATE_IMAGE_DATA";


export function AC_ADD_IMAGE(formdata) {
    return function (dispatch) {
        axios.post(URL.API.addUploadImage, formdata)
            .then(({ data }) => {
                dispatch({ type: ADD_IMAGE, payload: data });
                // console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_LIST_IMAGES() {
    return function (dispatch) {
        axios.get(URL.API.listImages)
            .then(({ data }) => {
                dispatch({ type: LIST_IMAGES, payload: data });
                console.log("=-=-=action-=-", data);
            });
    }
}
export function AC_VIEW_IMAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewImage, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_IMAGE, payload: data });

            });
    }
}
export function AC_DELETE_IMAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.deleteImage, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_IMAGE, payload: data });
            });
    }
}
export function AC_EDIT_IMAGE(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/categories/addUploadImage", formData)
            .then(({ data }) => {
                dispatch({ type: EDIT_IMAGE, payload: data });
            });
    }
}
export function AC_HANDLE_INPUT_CHANGE(name, value) {
    return function (dispatch) {
        dispatch({ type: UPDATE_IMAGE_DATA, name: name, value: value });

    }
}
