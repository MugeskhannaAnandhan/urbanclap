import axios from "axios";
import URL from "../../common/api";
const ADD_COUNTRY     = "ADD_COUNTRY";
const LIST_COUNTRIES  = "LIST_COUNTRIES";
const EDIT_COUNTRY    = "EDIT_COUNTRY";
const DELETE_COUNTRY  = "DELETE_COUNTRY";
const VIEW_COUNTRY    = "VIEW_COUNTRY";
const UPDATE_COUNTRY_DATA    = "UPDATE_COUNTRY_DATA";

export function AC_ADD_COUNTRY(formData) {
    return function (dispatch) {
        axios.post(URL.API.addCountry, formData)
            .then(({ data }) => {
                dispatch({ type: ADD_COUNTRY, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_LIST_COUNTRIES() {
    return function (dispatch) {
        axios.get(URL.API.listCountries)
            .then(({ data }) => {
                dispatch({ type: LIST_COUNTRIES, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_EDIT_COUNTRY(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/countries/addUpdateCountry", formData)
            .then(({ data }) => {
                dispatch({ type: EDIT_COUNTRY, payload: data });
            });
    }
}
export function AC_DELETE_COUNTRY(formData) {
    return function (dispatch) {
        axios.post(URL.API.deleteCountry, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_COUNTRY, payload: data });
            });
    }
}
export function AC_VIEW_COUNTRY(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewCountry, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_COUNTRY, payload: data });
            });
    }
}

export function AC_HANDLE_INPUT_CHANGE(name,value) {
    console.log("-==-name=-=",name);
    console.log("-==-value=-=",value);
    return function (dispatch) {
        dispatch({ type: UPDATE_COUNTRY_DATA, name:name, value:value });
        
    }
}
