import axios from "axios";
import URL from "../../common/api";
const ADD_CURRENCY     = "ADD_CURRENCY";
const LIST_CURRENCIES  = "LIST_CURRENCIES";
const EDIT_CURRENCY    = "EDIT_CURRENCY";
const DELETE_CURRENCY  = "DELETE_CURRENCY";
const VIEW_CURRENCY    = "VIEW_CURRENCY";
const UPDATE_CURRENCY_DATA    = "UPDATE_CURRENCY_DATA";

export function AC_ADD_CURRENCY(formData) {
    return function (dispatch) {
        axios.post(URL.API.addCurrency, formData)
            .then(({ data }) => {
                dispatch({ type: ADD_CURRENCY, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_LIST_CURRENCIES() {
    return function (dispatch) {
        axios.get(URL.API.listCurrencies)
            .then(({ data }) => {
                dispatch({ type: LIST_CURRENCIES, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_EDIT_CURRENCY(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/currencies/addUpdateCurrency", formData)
            .then(({ data }) => {
                dispatch({ type: EDIT_CURRENCY, payload: data });
            });
    }
}
export function AC_DELETE_CURRENCY(formData) {
    return function (dispatch) {
        axios.post(URL.API.deleteCurrency, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_CURRENCY, payload: data });
            });
    }
}
export function AC_VIEW_CURRENCY(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewCurrency, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_CURRENCY, payload: data });
            });
    }
}

export function AC_HANDLE_INPUT_CHANGE(name,value) {
    console.log("-==-name=-=",name);
    console.log("-==-value=-=",value);
    return function (dispatch) {
        dispatch({ type: UPDATE_CURRENCY_DATA, name:name, value:value });
        
    }
}
