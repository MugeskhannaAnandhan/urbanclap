import axios from "axios";
import URL from "../../common/api";
const ADD_FAQ    = "ADD_FAQ";
const LIST_FAQS  = "LIST_FAQS";
const EDIT_FAQ   = "EDIT_FAQ";
const DELETE_FAQ = "DELETE_FAQ";
const VIEW_FAQ   = "VIEW_FAQ";
const UPDATE_FAQ_DATA   = "UPDATE_FAQ_DATA";

export function AC_ADD_FAQ(formData) {
    return function (dispatch) {
        axios.post(URL.API.addFaq, formData)
            .then(({ data }) => {
                dispatch({ type: ADD_FAQ, payload: data });
                // console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_LIST_FAQS() {
    return function (dispatch) {
        axios.get(URL.API.listFaqs)
            .then(({ data }) => {
                dispatch({ type: LIST_FAQS, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_EDIT_FAQ(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/faqs/addUpdateFaq", formData)
            .then(({ data }) => {
                dispatch({ type: EDIT_FAQ, payload: data });
            });
    }
}
export function AC_DELETE_FAQ(formData) {
    return function (dispatch) {
        axios.post(URL.API.deleteFaq, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_FAQ, payload: data });
            });
    }
}
export function AC_VIEW_FAQ(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewFaq, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_FAQ, payload: data });
            });
    }
}
export function AC_HANDLE_INPUT_CHANGE(name,value) {
    return function (dispatch) {
        dispatch({ type: UPDATE_FAQ_DATA, name:name, value:value });
        
    }
}
