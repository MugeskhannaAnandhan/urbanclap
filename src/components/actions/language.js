import axios from "axios";
import URL from "../../common/api";
const ADD_LANGUAGE     = "ADD_LANGUAGE";
const LIST_LANGUAGES  = "LIST_LANGUAGES";
const EDIT_LANGUAGE    = "EDIT_LANGUAGE";
const DELETE_LANGUAGE  = "DELETE_LANGUAGE";
const VIEW_LANGUAGE    = "VIEW_LANGUAGE";
const UPDATE_LANGUAGE_DATA    = "UPDATE_LANGUAGE_DATA";

export function AC_ADD_LANGUAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.addLanguage, formData)
            .then(({ data }) => {
                dispatch({ type: ADD_LANGUAGE, payload: data });
            });
    }
}
export function AC_LIST_LANGUAGES() {
    return function (dispatch) {
        axios.get(URL.API.listLanguages)
            .then(({ data }) => {
                dispatch({ type: LIST_LANGUAGES, payload: data });
            });
    }
}
export function AC_EDIT_LANGUAGE(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/languages/addUpdateLanguage", formData)
            .then(({ data }) => {
                dispatch({ type: EDIT_LANGUAGE, payload: data });
            });
    }
}
export function AC_DELETE_LANGUAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.deleteLanguage, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_LANGUAGE, payload: data });
            });
    }
}
export function AC_VIEW_LANGUAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewLanguage, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_LANGUAGE, payload: data });
            });
    }
}

export function AC_HANDLE_INPUT_CHANGE(name,value) {
    return function (dispatch) {
        dispatch({ type: UPDATE_LANGUAGE_DATA, name:name, value:value });
        
    }
}
