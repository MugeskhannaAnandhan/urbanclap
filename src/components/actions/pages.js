import axios from "axios";
import URL from "../../common/api";
const ADD_PAGE    = "ADD_PAGE";
const LIST_PAGES  = "LIST_PAGES";
const EDIT_PAGE   = "EDIT_PAGE";
const DELETE_PAGE = "DELETE_PAGE";
const VIEW_PAGE   = "VIEW_PAGE";
const UPDATE_PAGE_DATA   = "UPDATE_PAGE_DATA";


export function AC_ADD_PAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.addPage, formData)
            .then(({ data }) => {
                dispatch({ type: ADD_PAGE, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_LIST_PAGES() {
    return function (dispatch) {
        axios.get(URL.API.listPages)
            .then(({ data }) => {
                dispatch({ type: LIST_PAGES, payload: data });
                console.log("=-=-=action-=-",data);
            });
    }
}
export function AC_EDIT_PAGE(formData) {
    return function (dispatch) {
        axios.post("http://localhost:8000/api/v1/pages/addUpdatePage", formData)
            .then(({ data }) => {
                dispatch({ type:EDIT_PAGE, payload: data });
            });
    }
}
export function AC_DELETE_PAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.deletePage, formData)
            .then(({ data }) => {
                dispatch({ type: DELETE_PAGE, payload: data });
            });
    }
}
export function AC_VIEW_PAGE(formData) {
    return function (dispatch) {
        axios.post(URL.API.viewPage, formData)
            .then(({ data }) => {
                dispatch({ type: VIEW_PAGE, payload: data });
            });
    }
}
export function AC_HANDLE_INPUT_CHANGE(name,value) {
    console.log("-==-name=-=",name);
    console.log("-==-value=-=",value);
    return function (dispatch) {
        dispatch({ type: UPDATE_PAGE_DATA, name:name, value:value });
        
    }
}
