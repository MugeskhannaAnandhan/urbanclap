import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {  AC_ADD_COUNTRY } from '../actions/country';
import { AC_LIST_COUNTRIES } from '../actions/country';
import swal from 'sweetalert';

class addCountry extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      codeError: false,
      codeCountError: false,
      color0: '',
      name: '',
      nameError: false,
      nameCountError: false,
      color1: '',
      status: '',
      statusError: false,
      color2: '',

    }
    this.validation = this.validation.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

  }

  validation() {
    const name = this.state.name;
    const code = this.state.code;
    const status = this.state.status
    if (name) {
      if (name.length < 3) {
        this.setState({ nameError: false, nameCountError: true, color0: "red" })
      }
      else {
        this.setState({ nameError: false, nameCountError: false, color0: "" })
      }
    }
    else {
      this.setState({ nameError: true, nameCountError: false, color0: "red" })
    }

    if (code) {
      if (code.length < 2) {
        this.setState({ codeError: false, codeCountError: true, color1: "red" })
      }
      else {
        this.setState({ codeError: false, codeCountError: false, color1: "" })
      }
    }
    else {
      this.setState({ codeError: true, codeCountError: false, color1: "red" })
    }

    if (status) {
      this.setState({ statusError: false, color2: '' })
    }
    else {
      this.setState({ statusError: true, color2: '1px solid red' })
    }
    if (name && code && status) {
      document.getElementById('addCountry').reset();
      swal("Country Added Successfully!", {
        buttons: false,
        timer: 2000,
      });
       this.setState({ name: '', code: '', status: '' });


    }
    else {
      document.getElementById('addCountry').reset();
      swal("Add your COUNTRY", {
        classNameName: "red-bg",
      });
    }

    const formData = {
      name: this.state.name,
      code: this.state.code,
      status: this.state.status
    }
    this.props.AC_ADD_COUNTRY(formData);
    console.log('-=value-=', formData)

  }
  handleInputChange(event) {
    const fieldId = event.target.id;
    const fieldValue = event.target.value;

    if (fieldId === "name") {
      this.setState({ name: fieldValue })
      if (fieldValue) {
        if (fieldValue.length < 3 ) {
          this.setState({ nameError: false, nameCountError: true, color0: 'red' })
        }
        else {
          this.setState({ nameError: false, nameCountError: false, color0: '' })
        }
      }
      else {
        this.setState({ nameError: true, nameCountError: false, color0: '' })
      }
    }

    if (fieldId === "code") {
      this.setState({ code: fieldValue })
      if (fieldValue) {
        if (fieldValue.length < 2) {
          this.setState({ codeError: false, codeCountError: true, color1: 'red' })
        }
        else {
          this.setState({ codeError: false, codeCountError: false, color1: '' })
        }
      }
      else {
        this.setState({ codeError: true, codeCountError: false, color1: '' })
      }
    }

    if (fieldId === "status") {
      this.setState({ status: fieldValue })
      if (fieldValue) {
        this.setState({ statusError: false, color2: '' })
      }
      else {
        this.setState({ statusError: true, color2: '1px solid red' })
      }
    }
  }


  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <h4 className="card-title">Add Country</h4>
                <form className="forms-sample" id="addCountry">
                  <div className="form-group">
                    <label for="exampleInputUsername1">NAME</label>
                    <input type="text" autoComplete='off' placeholder="Name" id="name" value={this.state.name} onChange={this.handleInputChange} style={{ borderColor: this.state.color0 }} className="form-control" />
                    {this.state.nameError ? <label className="mt-2" style={{ color: 'red' }}>Name is required</label> : ""}
                    {this.state.nameCountError ? <label className="mt-2" style={{ color: 'red' }}>Name should be atleast 3 characters</label> : ""}

                  </div>
                  <div className="form-group">
                    <label for="exampleInputUsername1">CODE</label>
                    <input type="text" autoComplete='off' placeholder="Code" id="code" value={this.state.code} onChange={this.handleInputChange} style={{ borderColor: this.state.color1 }} className="form-control" />
                    {this.state.codeError ? <label className="mt-2" style={{ color: 'red' }}>Code is required</label> : ""}
                    {this.state.codeCountError ? <label className="mt-2" style={{ color: 'red' }}>Code should be atleast 2 characters</label> : ""}

                  </div>
                  <div className="form-group">
                    <label for="exampleInputUsername1">STATUS</label>
                    <select className="form-control" id="status" style={{ backgroundColor: 'white' }}  onChange={this.handleInputChange} >
                      <option value="Status">Status</option>
                      <option value="active">Active</option>
                      <option value="inactive">Inactive</option>
                    </select>
                    {this.state.statusError ? <label className="mt-2" style={{ color: 'red' }}>Status is required</label> : ""}
                  </div>
                  <button type="button" className="btn btn-gradient-primary me-2" style={{
                    backgroundColor: 'blue',
                    color: 'white'
                  }} onClick={this.validation}>Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log('map state', state);
  return {
    countryReducer: state.countryReducer
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ AC_ADD_COUNTRY, AC_LIST_COUNTRIES }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(addCountry);
