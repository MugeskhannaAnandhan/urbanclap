import React from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { AC_ADD_COUNTRY, AC_LIST_COUNTRIES,AC_DELETE_COUNTRY } from '../actions/country';
import swal from 'sweetalert';
import { Redirect} from "react-router-dom";

class listCountries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editStatus: false,
            viewStatus: false
        }
        this.deleteCountry = this.deleteCountry.bind(this);
        this.editCountry = this.editCountry.bind(this);
        this.viewCountry = this.viewCountry.bind(this);
    }
    componentDidMount() {
        this.props.AC_LIST_COUNTRIES();
        this.props.AC_ADD_COUNTRY();
        this.props.AC_DELETE_COUNTRY();

    }

    editCountry(event){
        let countryId = event.target.id;
        this.setState({ editStatus: true, editId: countryId })
    }
    viewCountry(event){
        let countryId = event.target.id;
        this.setState({ viewStatus: true, viewId: countryId })
    }
    deleteCountry(event) {
        let countryId = event.target.id;
        var formData = {
            id: countryId,
        }
        swal({
            title: "Are you sure?",
            text: "Do you want to Delete the Country!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.AC_LIST_COUNTRIES();
                    this.props.AC_DELETE_COUNTRY(formData);
                    this.props.AC_LIST_COUNTRIES();
                    swal("Country Deleted Successfully!", {
                        buttons: false,
                        timer: 2000,
                      });
                } 
            });
    }

    render() {
        if (this.state.editStatus) {
            return <Redirect to={'/editCountry'+'/'+this.state.editId} />
        }
        if (this.state.viewStatus) {
            return <Redirect to={'/viewCountry'+'/'+this.state.viewId} />
        }

        var listCountry = this.props.countryReducer.listCountries;
        var activeCount = 0;
        var inactiveCount = 0;
        var resultArray = [];
        if (listCountry !== undefined) {
            if (listCountry.length > 0) {
                for (var i = 0; i < listCountry.length; i++) {
                    if (listCountry[i].status == "active") {
                        activeCount++;
                    }
                    else {
                        inactiveCount++;
                    }
                    resultArray.push(
                        <tr key={listCountry[i]._id}>
                            <th scope="row">{i + 1}</th>
                            <td>{listCountry[i].name}</td>
                            <td>{listCountry[i].code}</td>
                            <td>{listCountry[i].status}</td>
                            <td>
                                <i class="menu-icon mdi mdi-tooltip-edit text-primary me-2" id={listCountry[i]._id} onClick={this.editCountry} style={{fontSize: "22px"}}></i>
                                <i class="menu-icon mdi mdi mdi-view-list text-primary me-2" id={listCountry[i]._id} onClick={this.viewCountry} style={{fontSize: "25px" }}></i>
                                <i class="menu-icon mdi mdi-delete-forever text-primary me-2" id={listCountry[i]._id} onClick={this.deleteCountry} style={{fontSize: "25px" }}></i>
                            </td>
                        </tr>
                    )
                }
            }
        }

        

        return (
            <>
                <div className="content-wrapper">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="home-tab">
                                <div className="tab-content tab-content-basic">
                                    <div className="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="statistics-details d-flex align-items-center justify-content-between">
                                                    <div>
                                                        <p className="statistics-title">Total Country</p>
                                                        <h3 className="rate-percentage" style={{ textAlign: "center" }}>{listCountry.length}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Active</p>
                                                        <h3 className="rate-percentage">{activeCount}</h3>
                                                    </div>
                                                    <div>
                                                        <p className="statistics-title">Inactive</p>
                                                        <h3 className="rate-percentage">{inactiveCount}</h3>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table" style={{marginBottom: "275px"}}>
                    <thead>
                    <h4 className="card-title"><b>List Countries</b></h4>

                        <tr>
                            <th scope="col">S.No</th>
                            <th scope="col">Name</th>
                            <th scope="col">Code</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                        {resultArray}
                    </tbody>
                </table>
            </>

        );
    }
}



function mapStateToProps(state) {
    console.log('map state', state);
    return {
        countryReducer: state.countryReducer
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ AC_ADD_COUNTRY, AC_LIST_COUNTRIES,AC_DELETE_COUNTRY }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(listCountries);