import React from 'react';
import urbanclap from '../../images/Urban-Clap.jpg';
import logomini from '../../images/logo-mini.svg';
import face1 from '../../images/face1.jpg';
import face8 from '../../images/face8.jpg';
import face10 from '../../images/face10.jpg';
import face12 from '../../images/face12.jpg';

class Header extends React.Component {
    render() {
        return (
            <div>
                <nav className="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
                    <div className="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
                        <div className="me-3">
                            <button className="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
                                <span className="icon-menu"></span>
                            </button>
                        </div>
                        <div>
                            <a className="navbar-brand brand-logo" href="index.html">
                                <img src={urbanclap} alt="logo" />
                            </a>
                            <a className="navbar-brand brand-logo-mini" href="index.html">
                                <img src={logomini} alt="logo" />
                            </a>
                        </div>
                    </div>
                    <div className="navbar-menu-wrapper d-flex align-items-top">
                        <ul className="navbar-nav">
                            <li className="nav-item font-weight-semibold d-none d-lg-block ms-0">
                                <h1 className="welcome-text">Good Evening, <span className="text-black fw-bold">Admin</span></h1>
                            </li>
                        </ul>
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item dropdown">
                                <a className="nav-link count-indicator" id="countDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i className="icon-bell"></i>
                                    <span className="count"></span>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="countDropdown">
                                    <a className="dropdown-item py-3">
                                        <p className="mb-0 font-weight-medium float-left">You have 7 unread mails </p>
                                        <span className="badge badge-pill badge-primary float-right">View all</span>
                                    </a>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item preview-item">
                                        <div className="preview-thumbnail">
                                            <img src={face10} alt="image" className="img-sm profile-pic" />
                                        </div>
                                        <div className="preview-item-content flex-grow py-2">
                                            <p className="preview-subject ellipsis font-weight-medium text-dark">Marian Garner </p>
                                            <p className="fw-light small-text mb-0"> The meeting is cancelled </p>
                                        </div>
                                    </a>
                                    <a className="dropdown-item preview-item">
                                        <div className="preview-thumbnail">
                                            <img src={face12} alt="image" className="img-sm profile-pic" />
                                        </div>
                                        <div className="preview-item-content flex-grow py-2">
                                            <p className="preview-subject ellipsis font-weight-medium text-dark">David Grey </p>
                                            <p className="fw-light small-text mb-0"> The meeting is cancelled </p>
                                        </div>
                                    </a>
                                    <a className="dropdown-item preview-item">
                                        <div className="preview-thumbnail">
                                            <img src={face1} alt="image" className="img-sm profile-pic" />
                                        </div>
                                        <div className="preview-item-content flex-grow py-2">
                                            <p className="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                                            <p className="fw-light small-text mb-0"> The meeting is cancelled </p>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <li className="nav-item dropdown d-none d-lg-block user-dropdown">
                                <a className="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                                    <img className="img-xs rounded-circle" src={face8} alt="Profile image" /> </a>
                                <div className="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                                    <div className="dropdown-header text-center">
                                        <img className="img-md rounded-circle" src={face8} alt="Profile image" />
                                        <p className="mb-1 mt-3 font-weight-semibold">Allen Moreno</p>
                                        <p className="fw-light text-muted mb-0">allenmoreno@gmail.com</p>
                                    </div>
                                    <a className="dropdown-item"><i className="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> My Profile <span className="badge badge-pill badge-danger">1</span></a>
                                    <a className="dropdown-item"><i className="dropdown-item-icon mdi mdi-message-text-outline text-primary me-2"></i> Messages</a>
                                    <a className="dropdown-item"><i className="dropdown-item-icon mdi mdi-calendar-check-outline text-primary me-2"></i> Activity</a>
                                    <a className="dropdown-item"><i className="dropdown-item-icon mdi mdi-help-circle-outline text-primary me-2"></i> FAQ</a>
                                    <a className="dropdown-item"><i className="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Sign Out</a>
                                </div>
                            </li>
                        </ul>
                        <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
                            <span className="mdi mdi-menu"></span>
                        </button>
                    </div>
                    <div class="theme-setting-wrapper">
                        <div id="settings-trigger"><i class="ti-settings"></i></div>
                        <div id="theme-settings" class="settings-panel">
                            <i class="settings-close ti-close"></i>
                            <p class="settings-heading">SIDEBAR SKINS</p>
                            <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border me-3"></div>Light</div>
                            <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border me-3"></div>Dark</div>
                            <p class="settings-heading mt-2">HEADER SKINS</p>
                            <div class="color-tiles mx-0 px-4">
                                <div class="tiles success"></div>
                                <div class="tiles warning"></div>
                                <div class="tiles danger"></div>
                                <div class="tiles info"></div>
                                <div class="tiles dark"></div>
                                <div class="tiles default"></div>
                            </div>
                        </div>
                    </div>
                </nav>

            </div>
        );
    }
}

export default Header;
