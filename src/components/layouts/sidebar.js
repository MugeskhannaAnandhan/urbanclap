import React from "react";
import { Link } from "react-router-dom";

class Siedbar extends React.Component {
    render() {
        return (
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item">
                        <Link to="/dashboard" class="nav-link" >
                            <i class="mdi mdi-grid-large menu-icon"></i>
                            <span class="menu-title">DASHBOARD</span>
                        </Link>
                    </li>
                    <li class="nav-item nav-category">FAQ-Section</li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#faq" aria-expanded="false" aria-controls="faq">
                            <i class="menu-icon mdi mdi-comment-question-outline"></i>
                            <span class="menu-title">FAQS</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="faq">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addFaq" class="nav-link">ADD FAQ</Link></li>
                                <li class="nav-item"> <Link to="/listFaqs" class="nav-link">LIST FAQS</Link></li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#page" aria-expanded="false" aria-controls="page">
                            <i class="menu-icon mdi mdi-floor-plan"></i>
                            <span class="menu-title">PAGES</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="page">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addPage" class="nav-link">ADD PAGES</Link></li>
                                <li class="nav-item"> <Link to="/listPages" class="nav-link">LIST PAGES</Link></li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#country" aria-expanded="false" aria-controls="country">
                            <i class="menu-icon mdi mdi-flag-checkered"></i>
                            <span class="menu-title">COUNTRY</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="country">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addCountry" class="nav-link">ADD COUNTRY</Link></li>
                                <li class="nav-item"> <Link to="/listCountries" class="nav-link">LIST COUNTRIES</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#image" aria-expanded="false" aria-controls="image">
                            <i class="menu-icon mdi mdi-file-image"></i>
                            <span class="menu-title">CATEGORY</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="image">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addImage" class="nav-link">ADD CATEGORY</Link></li>
                                <li class="nav-item"> <Link to="/listImages" class="nav-link">LIST CATEGORY</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#currency" aria-expanded="false" aria-controls="image">
                            <i class="menu-icon mdi mdi-currency-inr"></i>
                            <span class="menu-title">CURRENCY</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="currency">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addCurrency" class="nav-link">ADD CURRENCY</Link></li>
                                <li class="nav-item"> <Link to="/listCurrencies" class="nav-link">LIST CURRENCY</Link></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#language" aria-expanded="false" aria-controls="language">
                            <i class="menu-icon mdi mdi-lead-pencil"></i>
                            <span class="menu-title">LANGUAGE</span>
                            <i class="menu-arrow"></i>
                        </a>
                        <div class="collapse" id="language">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <Link to="/addLanguage" class="nav-link">ADD LANGUAGE</Link></li>
                                <li class="nav-item"> <Link to="/listLanguages" class="nav-link">LIST LANGUAGES</Link></li>
                            </ul>
                        </div>
                    </li>


                </ul>

            </nav>
        );
    }
}

export default Siedbar;