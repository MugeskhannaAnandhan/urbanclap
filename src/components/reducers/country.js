const intialState = {
    addCountry     : [],
    listCountries  : [],
    editCountry    : [],
    deleteCountry  : [],
    countryInfo : {
        name     : '',
        code     : '',
        status   : '',
        id       : ''
    }
}
function countryReducer(state = intialState, action) {
    console.log("-=-reducer=-=-",action)
    switch (action.type) {
        case 'ADD_COUNTRY':
            return {
                ...state,
                addCountry: action.payload
            }
            break;
        case 'LIST_COUNTRIES':
            return {
                ...state,
                listCountries: action.payload.data
            }
            break;
        case 'EDIT_COUNTRY':
            return {
                ...state,
                editCountry: action.payload
            }
            break;
        case 'DELETE_COUNTRY':
            return {
                ...state,
                deleteCountry: action.payload
            }
            break;
        case 'VIEW_COUNTRY':
            return {
                ...state,
                countryInfo: action.payload
            }
            break;
            case 'UPDATE_COUNTRY_DATA':
            return Object.assign({},state,{
                countryInfo : {
                    ...state.countryInfo,
                    [action.name] : action.value
                }
            })
        default:
            return state;
            break;
    }
}
export default countryReducer;

