const intialState = {
    addCurrency    : [],
    listCurrencies  : [],
    editCurrency   : [],
    deleteCurrency  : [],
    currencyInfo : {
        name     : '',
        code     : '',
        status   : '',
        id       : ''
    }
}
function currencyReducer(state = intialState, action) {
    console.log("-=-reducer=-=-",action)
    switch (action.type) {
        case 'ADD_CURRENCY':
            return {
                ...state,
                addCurrency: action.payload
            }
            break;
        case 'LIST_CURRENCIES':
            return {
                ...state,
                listCurrencies: action.payload.data
            }
            break;
        case 'EDIT_CURRENCY':
            return {
                ...state,
                editCurrency: action.payload
            }
            break;
        case 'DELETE_CURRENCY':
            return {
                ...state,
                deleteCurrency: action.payload
            }
            break;
        case 'VIEW_CURRENCY':
            return {
                ...state,
                currencyInfo: action.payload
            }
            break;
            case 'UPDATE_CURRENCY_DATA':
            return Object.assign({},state,{
                currencyInfo : {
                    ...state.currencyInfo,
                    [action.name] : action.value
                }
            })
        default:
            return state;
            break;
    }
}
export default currencyReducer;

