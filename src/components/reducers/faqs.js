const intialState = {
    addFaq    : [],
    listFaqs  : [],
    editFaq   : [],
    deleteFaq : [],
    faqInfo : {
        question : '',
        answer   : '',
        status   : '',
        id       : ''
    }
}
function faqsReducer(state = intialState, action) {
    console.log("-=-reducer=-=-",action)
    switch (action.type) {
        case 'ADD_FAQ':
            return {
                ...state,
                addFaq: action.payload
            }
            break;
        case 'LIST_FAQS':
            return {
                ...state,
                listFaqs: action.payload.data
            }
            break;
        case 'EDIT_FAQ':
            return {
                ...state,
                editFaq: action.payload
            }
            break;
        case 'DELETE_FAQ':
            return {
                ...state,
                deleteFaq: action.payload
            }
            break;
        case 'VIEW_FAQ':
            return {
                ...state,
                faqInfo: action.payload
            }
            break;
            case 'UPDATE_FAQ_DATA':
            return Object.assign({},state,{
                faqInfo : {
                    ...state.faqInfo,
                    [action.name] : action.value
                }
            })
        default:
            return state;
            break;
    }
}
export default faqsReducer;

