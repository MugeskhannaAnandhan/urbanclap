const intialState = {
    addLanguage    : [],
    listLanguages  : [],
    editLanguage   : [],
    deleteLanguage  : [],
    languageInfo : {
        name     : '',
        code     : '',
        status   : '',
        id       : ''
    }
}
function languagesReducer(state = intialState, action) {
    console.log("-=-reducer=-=-",action)
    switch (action.type) {
        case 'ADD_LANGUAGE':
            return {
                ...state,
                addLanguage: action.payload
            }
            break;
        case 'LIST_LANGUAGES':
            return {
                ...state,
                listLanguages: action.payload.data
            }
            break;
        case 'EDIT_LANGUAGE':
            return {
                ...state,
                editLanguage: action.payload
            }
            break;
        case 'DELETE_LANGUAGE':
            return {
                ...state,
                deleteLanguage: action.payload
            }
            break;
        case 'VIEW_LANGUAGE':
            return {
                ...state,
                languageInfo: action.payload
            }
            break;
            case 'UPDATE_LANGUAGE_DATA':
            return Object.assign({},state,{
                languageInfo : {
                    ...state.languageInfo,
                    [action.name] : action.value
                }
            })
        default:
            return state;
            break;
    }
}
export default languagesReducer;

