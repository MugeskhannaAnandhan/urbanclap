const intialState = {
    addPage    : [],
    listPages  : [],
    editPage   : [],
    deletePage : [],
    pageInfo : {
        title       : '',
        slug        : '',
        description : '',
        id          : ''
    }
}
function pagesReducer(state = intialState, action) {
    console.log("-=-reducer=-=-",action)
    switch (action.type) {
        case 'ADD_PAGE':
            return {
                ...state,
                addPage: action.payload
            }
            break;
        case 'LIST_PAGES':
            return {
                ...state,
                listPages: action.payload.data
            }
            break;
        case 'EDIT_PAGE':
            return {
                ...state,
                editPage: action.payload
            }
            break;
        case 'DELETE_PAGE':
            return {
                ...state,
                deletePage: action.payload
            }
            break;
        case 'VIEW_PAGE':
            return {
                ...state,
                pageInfo: action.payload
            }
            break;
            case 'UPDATE_PAGE_DATA':
            return Object.assign({},state,{
                pageInfo : {
                    ...state.pageInfo,
                    [action.name] : action.value
                }
            })
        default:
            return state;
            break;
    }
}
export default pagesReducer;

