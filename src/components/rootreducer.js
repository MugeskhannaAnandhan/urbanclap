import { combineReducers } from "redux";
import faqsReducer from "../components/reducers/faqs";
import pagesReducer from "../components/reducers/pages";
import countryReducer from "../components/reducers/country";
import imagesReducer from "../components/reducers/category";
import currencyReducer from "../components/reducers/currency";
import languagesReducer from "../components/reducers/language";

const rootReducer = combineReducers({
    faqsReducer,
    pagesReducer,
    countryReducer,
    imagesReducer,
    currencyReducer,
    languagesReducer,
})
export default rootReducer;
